package main

import (
	"context"
	"gitlab.com/ngilmant/training-go-2/pkg/service"
	"gitlab.com/ngilmant/training-go-2/pkg/signals"
	"syscall"
)


func main() {

	ctx := signals.WithCancelSignals(
		context.Background(),
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGHUP,
	)

	if err := service.Start(ctx); err != nil {
		panic(err)
	}
}



