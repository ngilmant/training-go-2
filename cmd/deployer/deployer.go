package main

import (
	"context"
	"github.com/gorilla/mux"
	"gitlab.com/ngilmant/training-go-2/pkg/deployer"
	"gitlab.com/ngilmant/training-go-2/pkg/signals"
	"syscall"
)

func main() {

	mux := mux.NewRouter()

	ctx := signals.WithCancelSignals(
		context.Background(),
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGHUP,
	)

	server := deployer.CreateServer(mux)

	if err := server.Start(ctx); err != nil {
		panic(err)
	}
}
