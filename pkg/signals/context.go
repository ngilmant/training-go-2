package signals

import (
	"context"
	"errors"
	"os"
	"os/signal"
	"sync"
)

var Canceled = errors.New("signal received")

type signalCancelCtx struct {
	context.Context
	mu      sync.Mutex
	err     error
	once    sync.Once
	signal  os.Signal
	sigchan chan os.Signal
}

// WithCancelSignals returns a copy of parent which the value can context cancel by
// signals.
//
// Context cancellation is executed after receiving the signal.
func WithCancelSignals(parent context.Context, sig ...os.Signal) context.Context {
	cctx, cancel := context.WithCancel(parent)

	sigctx := newSignalCancelCtx(cctx)
	signal.Notify(sigctx.sigchan, sig...)
	go func() {
		for {
			select {
			case <-sigctx.Done():
				return
			case sigctx.signal = <-sigctx.sigchan:
				sigctx.once.Do(func() {
					sigctx.err = Canceled
					cancel()
				})
			}
		}
	}()
	return sigctx
}

// newSignalCancelCtx returns an initialized signalCancelCtx.
func newSignalCancelCtx(parent context.Context) *signalCancelCtx {
	return &signalCancelCtx{
		Context: parent,
		sigchan: make(chan os.Signal, 1),
	}
}
