package grpcbase

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"net"
)

//TODO Move this in a Configuration struct
const (
	grpcPort = 10000
)

type Server struct {
	grpcPort	int32
	grpcServer  *grpc.Server
}

func NewServer() (*Server, error) {
	server := &Server{
		grpcPort: grpcPort,
	}
	err := server.createGrpcServer()
	if err != nil {
		return nil, err
	}
	return server, nil
}

func (s *Server) createGrpcServer() error {
	s.grpcServer = grpc.NewServer()
	return nil
}

func (s *Server) GrpcServer() *grpc.Server {
	return s.grpcServer
}

func (s *Server) ListenAndServe(ctx context.Context) error {
	addr := fmt.Sprintf(":%d", s.grpcPort)
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		panic(fmt.Errorf("unable to start grpc server %v", err))
	}
	fmt.Printf("start grpc API server on Port %d \n", s.grpcPort)
	go func() {
		err := s.grpcServer.Serve(lis)
		if err != nil {
			panic(err)
		}
	}()
	<-ctx.Done()
	fmt.Printf("stop grpc server on port %d \n", s.grpcPort)
	return nil
}

func (s *Server) DialOpts() ([]grpc.DialOption, error) {
	opts := make([]grpc.DialOption, 0)
	opts = append(opts, grpc.WithInsecure())
	return opts,  nil
}