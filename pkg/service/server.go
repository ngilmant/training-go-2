package service

import (
	"context"
	"fmt"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	gw "gitlab.com/ngilmant/training-go-2/pkg/api"
	"gitlab.com/ngilmant/training-go-2/pkg/backend"
	"gitlab.com/ngilmant/training-go-2/pkg/grpcbase"
	"net/http"
	"time"
)

//TODO Move this in a Configuration struct
const (
	restPort = 8080
	grpcPort = 10000
)

type server struct {
	grpcPort              int32
	restPort              int32
}

func Start(ctx context.Context) error {
	server, err := newServer()
	if err != nil {
		return err
	}
	go server.Start(ctx)
	<-ctx.Done()
	time.Sleep(1 * time.Second)
	return nil
}

func newServer() (*server, error) {
	s := &server{
		grpcPort: grpcPort,
		restPort: restPort,
	}
	return s, nil
}

func (s *server) Start(ctx context.Context) {
	go s.startGrpcServer(ctx)
	go s.startGrpcGatewayServer(ctx)
	//go s.startManagementServer(ctx)
}

func (s *server) startGrpcServer(ctx context.Context) {
	grpcServer, err := grpcbase.NewServer()
	if err != nil {
		panic(err)
	}
	m := backend.NewManager()
	m.Start(ctx)
	gw.RegisterCacheServiceServer(grpcServer.GrpcServer(), CreateCacheService(m))
	go func() {
		err := grpcServer.ListenAndServe(ctx)
		if err != nil {
			panic(err)
		}
	}()
	<-ctx.Done()
	grpcServer.GrpcServer().Stop()
}

func (s *server) startGrpcGatewayServer(ctx context.Context) {
	mux := runtime.NewServeMux()
	grpcServer, err := grpcbase.NewServer()
	if err != nil {
		panic(err)
	}
	opts, err := grpcServer.DialOpts()
	if err != nil {
		panic(err)
	}
	gRPCAddr := fmt.Sprintf("localhost:%d", s.grpcPort)
	dialAddr := fmt.Sprintf("passthrough://localhost/%s", gRPCAddr)
	err = gw.RegisterCacheServiceHandlerFromEndpoint(ctx, mux, dialAddr, opts)
	fmt.Sprintf("Start http API server on Port %d\n", s.restPort)
	server := &http.Server{Addr: fmt.Sprintf(":%d", s.restPort), Handler: mux}
	go func() {
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			panic(err)
		}
	}()
	<-ctx.Done()
	fmt.Sprintf("http API server stopped")
	server.Close()
}
