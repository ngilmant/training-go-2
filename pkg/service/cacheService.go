package service

import (
	"context"
	"errors"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	gw "gitlab.com/ngilmant/training-go-2/pkg/api"
	"gitlab.com/ngilmant/training-go-2/pkg/backend"
	"google.golang.org/grpc"
	"net/http"
	"strings"
)

type CacheService struct {
	CM *backend.Manager
}


func CreateCacheService(manager *backend.Manager) (*CacheService) {
	return &CacheService{
		manager,
	}
}

func GrpcHandlerFunc(grpcServer *grpc.Server, otherHandler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.ProtoMajor == 2 && strings.Contains(r.Header.Get("Content-Type"), "application/grpc") {
			grpcServer.ServeHTTP(w, r)
		} else {
			otherHandler.ServeHTTP(w, r)
		}
	})
}

/*GRPC*/
func (cs *CacheService) AddItem(ctx context.Context, in *gw.ItemRequest) (*gw.Item, error) {
	fmt.Printf("CacheService.AddItem called \n")

	key := in.Key
	value := in.Item.Value

	item, err := cs.CM.Add(key, value)
	if err != nil {
		return new(gw.Item), errors.New(fmt.Sprintf("Error crating an Item %v\n", err))
	}

	value = item.Value

	return &gw.Item{
		Value: value,
	}, nil
}

func (cs *CacheService) ListItems(ctx context.Context, in *empty.Empty) (*gw.ItemList, error) {
	fmt.Printf("CacheService.ListItems called \n")
	l := cs.CM.List()
	result := &gw.ItemList{
		Items : make(map[string]*gw.Item, len(l)),
	}

	for i, e := range l {
		result.Items[i] = &gw.Item{Value: e.Value}
	}
	return result, nil
}

func (cs *CacheService) FindItem(ctx context.Context, in *gw.ItemRequest) (*gw.Item, error) {
	fmt.Printf("CacheService.FindItem called \n")

	i, err := cs.CM.Find(in.Key)
	if err != nil{
		return new(gw.Item), errors.New(fmt.Sprintf("Error Finding an Item: %v\n", err))
	}

	return  &gw.Item{
		Value: i.Value,
	}, nil
}

func (cs *CacheService) UpdateRequest(ctx context.Context, in *gw.ItemRequest) (*empty.Empty, error) {
	fmt.Printf("CacheService.UpdateRequest called \n")
	r := backend.ItemRequest{
		Key: in.Key,
		Element: backend.Item{
			Value: in.Item.Value,
		},
	}
	cs.CM.UpdateChannel() <- r
	return  new(empty.Empty), nil
}

func (cs *CacheService) DeleteRequest(ctx context.Context, in *gw.ItemRequest) (*empty.Empty, error) {
	fmt.Printf("CacheService.DeleteRequest called \n")
	cs.CM.DeleteChannel() <- in.Key
	return new(empty.Empty), nil
}
