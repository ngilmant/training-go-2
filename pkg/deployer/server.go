package deployer

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"time"
)

const (
	restPort = 8080
)

type Server struct {
	server		*http.Server
}

func CreateServer(router *mux.Router) *Server {

	h := createHandler()
	h.SetupRoute(router)

	srv := &http.Server{
		Addr:         fmt.Sprintf(":%d", restPort),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  120 * time.Second,
		Handler:      router,
	}

	return &Server{
		srv,
	}
}

func (s *Server) Start(ctx context.Context) error {

	go func() {
		err := s.server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			panic(err)
		}
	}()
	fmt.Printf("Server started on port :%d\n", restPort)
	<- ctx.Done()
	s.server.Close()
	fmt.Printf("Server stopped\n")
	return nil
}

