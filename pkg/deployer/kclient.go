package deployer

import (
	"fmt"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/apps/v1"
	apiv1 "k8s.io/api/core/v1"
	"k8s.io/api/extensions/v1beta1"
	"k8s.io/apimachinery/pkg/api/errors"
	mach1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"os"
)

type Kclient struct {
	k8s		kubernetes.Interface
}


func CreateK8sClient() *Kclient {

	config, err :=  kubeConfig()
	if err != nil {
		fmt.Printf("Error occurs during configuraing kube client %v\n", err)
		panic(err)
	}

	kclient := kubernetes.NewForConfigOrDie(config)
	info, err := kclient.Discovery().ServerVersion()
	if err != nil {
		fmt.Printf("unable to connect kubernetes api %v\n", err)
		panic(err)
	}

	fmt.Printf("[kubernetes-client] connected to kubernetes version %s.%s\n", info.Major, info.Minor)
	return &Kclient{
		kclient,
	}
}

func kubeConfig() (*rest.Config, error) {
	if kubeConfig := os.Getenv("KUBECONFIG"); len(kubeConfig) > 0 {
		return clientcmd.BuildConfigFromFlags("", kubeConfig)
	}
	return rest.InClusterConfig()
}

func (k *Kclient) makeDeploy(req *DeployRequest) (*v1.Deployment, error) {
	deployment,  err := CreateDeployment(k, req)
	if err != nil {
		fmt.Printf("Error occured during the creation of the Deployment %v\n", err)
		return nil, err
	}
	return deployment, nil
}

func (k *Kclient) makeService(req *DeployRequest) (*apiv1.Service, error) {
	svc, err := CreateService(k, req)
	if err != nil {
		fmt.Printf("Error occured during the creation of the Service %v\n", err)
		return nil, err
	}
	return svc, nil
}

func (k *Kclient) makeIngress(req *DeployRequest) (*v1beta1.Ingress, error) {
	ing, err := CreateIngress(k, req)
	if err != nil {
		fmt.Printf("Error occured during the creation of the Ingress %v\n", err)
		return nil, err
	}
	return ing, nil
}


func (k *Kclient) deleteAll(appName string, namespace string) map[string]string {
	statusList := make(map[string]string)

	//Delete Deployment
	err := k.deleteDeployment(appName, namespace)
	if err != nil {
		statusList[fmt.Sprintf("%v-deployment", appName)] = fmt.Sprintf("%v", err)
	} else {
		statusList[fmt.Sprintf("%v-deployment", appName)] = "OK"
	}

	//Delete Service.
	err = k.deleteService(appName, namespace)
	if err != nil {
		statusList[fmt.Sprintf("%v-service", appName)] = fmt.Sprintf("%v", err)
	} else {
		statusList[fmt.Sprintf("%v-service", appName)] = "OK"
	}

	//Delete Ingress.
	err = k.deleteIngress(appName, namespace)
	if err != nil {
		statusList[fmt.Sprintf("%v-ingress", appName)] = fmt.Sprintf("%v", err)
	} else {
		statusList[fmt.Sprintf("%v-ingress", appName)] = "OK"
	}

	return statusList
}

/*
	Delete Deployment
*/
func (k *Kclient) deleteDeployment(appName string, namespace string)  error {

	namespace = selectNamespace(namespace)
	deploymentsClient := k.k8s.AppsV1().Deployments(namespace)

	options := &metav1.DeleteOptions{
	}

	err := deploymentsClient.Delete(fmt.Sprintf("%v-deployment", appName), options)
	if err != nil {
		fmt.Printf("Error occured during the deletion of the deployment [%v-deployment] - cause: %v", appName, err)
		return err
	}

	return nil
}

/*
	Delete Service
*/
func (k *Kclient) deleteService(appName string, namespace string) error {

	namespace = selectNamespace(namespace)
	servicesClient := k.k8s.CoreV1().Services(namespace)

	options := &metav1.DeleteOptions{
	}

	err := servicesClient.Delete(fmt.Sprintf("%v-service", appName), options)
	if err != nil {
		fmt.Printf("Error occured during the deletion of the service [%v-service] - cause: %v", appName, err)
		return err
	}

	return nil
}

/*
	Delete Ingress
*/
func (k *Kclient) deleteIngress(appName string, namespace string) error {

	namespace = selectNamespace(namespace)
	ingressesClient := k.k8s.ExtensionsV1beta1().Ingresses(namespace)

	//empty options....
	options := &mach1.DeleteOptions{
	}

	err := ingressesClient.Delete(fmt.Sprintf("%v-ingress", appName), options)
	if err != nil {
		fmt.Printf("Error occured during the deletion of the ingress [%v-ingress] - cause: %v", appName, err)
		return err
	}

	return nil
}

/*
	Create a Deployment
 */
func CreateDeployment(k *Kclient, req *DeployRequest) (*v1.Deployment, error) {

	namespace := selectNamespace(req.Namespace)
	deploymentsClient := k.k8s.AppsV1().Deployments(namespace)


	deployment := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name: fmt.Sprintf("%v-deployment", req.AppName),
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: &req.Replicas,
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string {
					"app" : req.AppName,
				},
			},
			Template: apiv1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: map[string]string{
						"app": req.AppName,
					},
				},
				Spec: apiv1.PodSpec{
					Containers: []apiv1.Container{
						{
							Name: req.AppName,
							Image: req.ImgName,
							Ports: []apiv1.ContainerPort{
								{
									Name: "http",
									Protocol: apiv1.ProtocolTCP,
									ContainerPort: req.ContainerPort,
								},
							},
						},
					},
				},
			},
		},
	}

	var result *v1.Deployment
	var err error

	getOptions := metav1.GetOptions{
	}
	//Check if Deplyment already Exists
	existingDep, err := deploymentsClient.Get(fmt.Sprintf("%v-deployment", req.AppName), getOptions)
	if err != nil {
		if errors.IsNotFound(err) {
			existingDep = nil
		}
	}

	if existingDep != nil {
		//Update Deployment
		fmt.Println("Updating deployment...")
		result, err  = deploymentsClient.Update(deployment)
		if err != nil {
			return nil, err
		}
	} else {
		// Create Deployment
		fmt.Println("Creating deployment...")
		result, err = deploymentsClient.Create(deployment)
		if err != nil {
			return nil, err
		}
	}

	fmt.Printf("Created/Updated deployment %q.\n", result.GetObjectMeta().GetName())

	return result, nil
}

/*
	Create a Service
*/
func CreateService(k *Kclient, req *DeployRequest) (*apiv1.Service, error) {

	namespace := selectNamespace(req.Namespace)
	servicesClient := k.k8s.CoreV1().Services(namespace)

	service := &apiv1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name: fmt.Sprintf("%v-service", req.AppName),
			Labels: map[string]string {
					"app": req.AppName,
				},
		},
		Spec: apiv1.ServiceSpec {
			Selector: map[string]string {
				"app": req.AppName,
			},
			Ports: []apiv1.ServicePort{
				{
					Port: req.IngressPort,
					Protocol: apiv1.ProtocolTCP,
					TargetPort: intstr.FromInt(int(req.ContainerPort)),
				},
			},
		},
	}

	var result *apiv1.Service

	getOptions := metav1.GetOptions{
	}
	existingSvc, err := servicesClient.Get(fmt.Sprintf("%v-service", req.AppName), getOptions)
	if err != nil {
		if errors.IsNotFound(err) {
			existingSvc = nil
		}
	}

	if existingSvc != nil {
		service.ObjectMeta.ResourceVersion = existingSvc.ObjectMeta.ResourceVersion
		service.Spec = existingSvc.Spec
		// Create Service
		fmt.Println("Updating service...")
		result, err = servicesClient.Update(service)
		if err != nil {
			return nil, err
		}
	} else {
		// Create Service
		fmt.Println("Creating service...")
		result, err = servicesClient.Create(service)
		if err != nil {
			return nil, err
		}
	}

	fmt.Printf("Created/Updated deployment %q.\n", result.GetObjectMeta().GetName())

	return result, nil
}

/*
	Create Ingress
 */
func CreateIngress(k *Kclient, req *DeployRequest) (*v1beta1.Ingress, error) {

	namespace := selectNamespace(req.Namespace)
	ingressesClient := k.k8s.ExtensionsV1beta1().Ingresses(namespace)


	ingress := &v1beta1.Ingress{
		ObjectMeta: metav1.ObjectMeta{
			Name: fmt.Sprintf("%v-ingress", req.AppName),
			Labels: map[string]string {
				"name" : fmt.Sprintf("%v-ingress", req.AppName),
				"app": req.AppName,
			},
			Annotations: map[string]string {
				"nginx.ingress.kubernetes.io/ssl-redirect": "false",
			},
		},
		Spec: v1beta1.IngressSpec{
			Rules: []v1beta1.IngressRule{
				{
					Host: req.IngressHost,
					IngressRuleValue: v1beta1.IngressRuleValue {
						HTTP: &v1beta1.HTTPIngressRuleValue{
							Paths: []v1beta1.HTTPIngressPath{
								{
									Path: req.IngressPath,
									Backend: v1beta1.IngressBackend{
										ServiceName: fmt.Sprintf("%v-service", req.AppName),
										ServicePort: intstr.FromInt(int(req.IngressPort)),
									},
								},
							},
						},
					},
				},
			},
		},
	}

	var result *v1beta1.Ingress

	getOptions := metav1.GetOptions{
	}

	existingIng, err := ingressesClient.Get(fmt.Sprintf("%v-ingress", req.AppName), getOptions)
	if err != nil {
		if errors.IsNotFound(err) {
			existingIng = nil
		}
	}

	if existingIng != nil {
		// Update Ingress
		fmt.Println("Updating ingress...")
		result, err = ingressesClient.Update(ingress)
		if err != nil {
			return nil, err
		}
	} else {
		// Create Ingress
		fmt.Println("Creating ingress...")
		result, err = ingressesClient.Create(ingress)
		if err != nil {
			return nil, err
		}
	}

	fmt.Printf("Created/Updated ingress %q.\n", result.GetObjectMeta().GetName())

	return result, nil
}


func selectNamespace(namespace string) string {
	if len(namespace) > 0{
		return  namespace
	} else {
		return apiv1.NamespaceDefault
	}
}