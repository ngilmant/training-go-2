package deployer

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
)

type Handler struct {
	kclient   *Kclient
}

type DeployRequest struct {
	AppName	  		string `json:"appName"`
	ImgName   		string `json:"imgName"`
	Namespace       string `json:"namespace"`
	Replicas  		int32  `json:"replicas"`
	ContainerPort  	int32  `json:"containerPort"`
	IngressPath		string `json:"ingressPath"`
	IngressPort		int32  `json:"ingressPort"`
	IngressHost     string `json:"ingressHost"`
}

func createHandler() *Handler{
	k8sClient := CreateK8sClient()
	return &Handler{k8sClient}
}

func (h *Handler) SetupRoute(mux *mux.Router) {
	mux.HandleFunc("/deployment", h.deploy).Methods(http.MethodPost)
	mux.HandleFunc("/service", h.service).Methods(http.MethodPost)
	mux.HandleFunc("/ingress", h.ingress).Methods(http.MethodPost)
	mux.HandleFunc("/deployAll", h.deployAll)
	mux.HandleFunc("/deleteAll/{appName}/{namespace}", h.deleteAll).Methods(http.MethodDelete)
}


func (h *Handler) deployAll(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("deploy All end-point called\n")
	deployReq, err := parseDeployRequest(r)
	if err != nil {
		http.Error(w, fmt.Sprint(err), http.StatusBadRequest)
		return
	}

	statusList := make(map[string]string)

	//make Deployment.
	_, err = h.kclient.makeDeploy(deployReq)
	if err != nil {
		statusList[fmt.Sprintf("%v-deployment", deployReq.AppName)] = fmt.Sprintf("%v", err)
	} else {
		statusList[fmt.Sprintf("%v-deployment", deployReq.AppName)] = "OK"
	}

	//make Service.
	_, err = h.kclient.makeService(deployReq)
	if err != nil {
		statusList[fmt.Sprintf("%v-service", deployReq.AppName)] = fmt.Sprintf("%v", err)
	} else {
		statusList[fmt.Sprintf("%v-service", deployReq.AppName)] = "OK"
	}

	//make Ingress.
	_, err = h.kclient.makeIngress(deployReq)
	if err != nil {
		statusList[fmt.Sprintf("%v-ingress", deployReq.AppName)] = fmt.Sprintf("%v", err)
	} else {
		statusList[fmt.Sprintf("%v-ingress", deployReq.AppName)] = "OK"
	}

	json.NewEncoder(w).Encode(statusList)
}

func (h *Handler) deploy(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("create deployment end-point called\n")
	deployReq, err := parseDeployRequest(r)
	if err != nil {
		http.Error(w, fmt.Sprint(err), http.StatusBadRequest)
		return
	}
	//Create Deployment.
	_, err = h.kclient.makeDeploy(deployReq)
	if err != nil {
		http.Error(w, fmt.Sprint(err), http.StatusInternalServerError)
		return
	}
}

func (h *Handler) service(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("create service end-point called\n")
	deployReq, err := parseDeployRequest(r)
	if err != nil {
		http.Error(w, fmt.Sprint(err), http.StatusBadRequest)
		return
	}
	//Create Service.
	_, err = h.kclient.makeService(deployReq)
	if err != nil {
		http.Error(w, fmt.Sprint(err), http.StatusInternalServerError)
		return
	}
}

func (h *Handler) ingress(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("create ingress end-point called\n")
	deployReq, err := parseDeployRequest(r)
	if err != nil {
		http.Error(w, fmt.Sprint(err), http.StatusBadRequest)
		return
	}
	//Create Ingress
	_, err = h.kclient.makeIngress(deployReq)
	if err != nil {
		http.Error(w, fmt.Sprint(err), http.StatusInternalServerError)
		return
	}
}

func (h *Handler) deleteAll(w http.ResponseWriter, r *http.Request) {
	appName, ok := mux.Vars(r)["appName"]
	if !ok {
		http.Error(w, fmt.Sprintf("No Path parameter found in your request for appName: %v\n", appName), http.StatusBadRequest)
		return
	}

	nameSpace, ok := mux.Vars(r)["namespace"]

	statusList := h.kclient.deleteAll(appName, nameSpace)
	json.NewEncoder(w).Encode(statusList)
}

func parseDeployRequest(r *http.Request) (req *DeployRequest, err error) {
	b, _ := ioutil.ReadAll(r.Body)
	err = json.Unmarshal(b, &req)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Error Unmarshalling Request : %v\n", err))
	}
	return req, nil
}