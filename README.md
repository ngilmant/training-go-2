# training-go-2

GO Training Tour Exercise 2 (intermediate)


## Niveau

 

Intermediate

 

## Topics 

 
·      GRPC

·      GRPC Gateway

·      Kubernetes GO Client

·      Kubernetes Standard Objects (deployment, service, ingress, service account, network policy)  

 

 

## Content

 

The Exercise has 2 applications:

 

-       a GRPC server (gs)

-       a k8s deployer application (gd)

 

Both applications must run on kubernetes and be reachable from the outside through an ingress controller

 

Accesses URL's are (e.g.) :

 
http://go-training-comp.ocf-marvin.net

http://go-training-deployer.ocf-marvin.net

 

 

## Server

 
Start from the server created during the 1st training (https://gitlab.com/ngilmant/training-go) and migrate it to an http server over GRPC (grpc-gateway).
GRPC has to be accessible through REST from outside the cluster (through an ingress controller)
 

## Deployer

 
Crate an application able to deploy component on Kubernetes?

This application must implement the following services API the way you want: 

* Deploy a component (deployment, service, ingress)
* Scale a component
* Delete a component
 
!! The deployer must also work on kubernetes. 
Take care of the service account, role and binding.
Do not use the cluster role and does not create rights that are not strictly necessary for the deployer application.
 