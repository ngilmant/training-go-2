#Enable GO Mod
GO111MODULE=on

VERSION := 0.1.0-SNAPSHOT

# ##########################################
# Generate the grpc Go interface from .proto
# ##########################################

gen:
	protoc \
	-I$(GOPATH)/src \
	-I./pkg/third_party/googleapis \
	--go_out=plugins=grpc:$(GOPATH)/src \
	--grpc-gateway_out=logtostderr=true:$(GOPATH)/src \
	--swagger_out=logtostderr=true:$(GOPATH)/src \
	$(GOPATH)/src/gitlab.com/ngilmant/training-go-2/pkg/api/api-operation.proto


# ###################
# BUILD THE GO Server (gs)
# ###################

gs:
	rm -rf $(GOPATH)/bin/server
	go build -v -i -o $(GOPATH)/bin/server ./cmd/server

gd:
	rm -rf $(GOPATH)/bin/deployer
	go build -v -i -o $(GOPATH)/bin/deployer ./cmd/deployer

# local build, exe will be in bin directory
build-gs:
	rm -rf bin/server
	go build -v -i -o bin/server ./cmd/server

# build linux cross compilation
build-linux-gs:
	rm -rf bin/linux/server
	GOOS=linux GOARCH=amd64 go build -v -i -o bin/linux/server/app ./cmd/server

build-image-gs:
	docker build -t registry.gitlab.com/ngilmant/training-go-2/cache-server -f Dockerfile-gs .

# #####################
# BUILD THE GO Deployer (gd)
# #####################
build-gd:
	rm -rf bin/deployer
	go build -v -i -o bin/deployer ./cmd/deployer

build-linux-gd:
	rm -rf bin/linux/deployer
	GOOS=linux GOARCH=amd64 go build -v -i -o bin/linux/deployer/app ./cmd/deployer

build-image-gd:
	docker build -t registry.gitlab.com/ngilmant/training-go-2/deployer-server -f Dockerfile-gd .